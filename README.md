# privacystorm

Eine kleine Website um "DDOS-Datenanfragen" an deiner Schule durchzuführen. Schüler*innen klicken dann einfach nur auf einen Knopf um Datenanfragen zu senden und können so die Verantwortlichen zum umdenken bewegen.



![image alt text](screenshot.jpg "Screenshot") 

Bitte denke daran die JS- und CSS-Dateien lokal einzubinden! Wenn deine Seite über keine Suchmaschine gefunden werden soll solltest du die robots-Datei verwenden, die davor zumeist schützt.